const request = require('request')

module.exports = {
    /*
    ** This method returns a promise
    ** which gets resolved or rejected based
    ** on the result from the API
    */
    make_API_call : function(url){
        return new Promise((resolve, reject) => {
            request(
                {
                    url : url,
                    json: true,
                    headers : {
                        "X-TheySaidSo-Api-Secret" : "fJAn78Gl_rF_8fNjStseDAeF"
                    }
                },
                function (err, res, body) {
                    if (err) reject(err)
                    resolve(body)
                });
        })
    }
}
